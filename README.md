# Code Quality Control Suite for Python3

    "Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live."
    ~ John F. Woods

This repo is a base for python project with code quality control tools. From PEP8 style guide to unit test, if the code is going to be deployed on production, why not make it **production-quality**.

## Getting Started  
- Download this repository
- Unzip repo and change folders name to build on this base, or copy all files into destination project folder
- Create virtual environment in env folder using venv
- If using VSCODE, change settings.json in .vscode accordingly
- Activate virtual environment, going into /env/Scripts/ and run activate.bat
- Run `pip install -r requirements.txt`


## Tools in this project  

- **flake8** check for PEP8 style guide  
`flake8 <foldername> <foldername> ....` 

- **black** refactor code to conform PEP8  
`black <foldername> <foldername> ....`  

- **pytest** to run unit test in \tests folder  
`pytest tests -v`

- **Sphinx** is a docs generator form python docstring.
    - Navigate to `./docs` and run `./make html` to build html docs.
    - See `./docs/build/html/index.html` for example, open it with a web browser of your choice.
    - Compare the html file with `./docs/conf.py`, change project, copyright, and author.
    - Compare the html file with `./docs/index.rst`, this file is using reStructuredText syntax.
        - Use `*******` to indicate first level header.
        - Use `=======` to indicate second level header.
        - `.. automodule:: path.to.package`  
        ` :members:` is to generate document from python docstring.
        - See `./app/packages/example_packages/example_function.py` for docstring example. (It support either docstrings of NumPy style or Google style)
        