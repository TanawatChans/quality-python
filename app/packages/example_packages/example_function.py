"""
example_packages.example_function
"""

from typing import Union


def plus_function(num_1: Union[int, float], num_2: Union[int, float]) -> Union[int, float]:
    """Plus function.

    Args:
        num_1: The first number.
        num_2: The second number.

    Returns:
        Result of num_1 + num_2

    """
    return num_1 + num_2


def minus_function(num_1: Union[int, float], num_2: Union[int, float]) -> Union[int, float]:
    """Minus function.

    Args:
        num_1: The first number.
        num_2: The second number.

    Returns:
        Result of num_1 - num_2

    """
    return num_1 - num_2
