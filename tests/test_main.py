from app.packages.example_packages import example_function


def test_plus_function():
    assert example_function.plus_function(2, 3) == 5


def test_minus_function():
    assert example_function.minus_function(2, 3) == -1

